import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';
import 'package:trainer_app/icons/custom_icons_icons.dart';
import 'package:trainer_app/main.dart';
import 'package:trainer_app/models/InstrClass.dart';

import 'models/InstrDetail.dart';
import 'models/StdList.dart';
import 'classattend_page.dart';
import 'services/services.dart';

class SchedulePage extends StatefulWidget {
  final String instrCode;
  final String instrName;
  final List<InstrClassRes> listClsData;
  SchedulePage({Key key, this.instrCode, this.instrName, this.listClsData})
      : super(key: key);
  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<SchedulePage> {
  DateTime _selectedDay;
  List<InstrRes> instrRes;
  List<InstrClassRes> listInstrClass;
  String instrCode;
  String instrName;
  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();
    _getInstrDetail();
    _getInstrClsList();
  }

  void _getInstrDetail() async {
    if (widget.instrName != null) {
      setState(() {
        instrName = widget.instrName;
      });
    } else {
      instrRes = await Service.getInstrDetail("InstrDetail", widget.instrCode);
      setState(() {
        instrName = instrRes[0].instName;
      });
    }
  }

  void _getInstrClsList() async {
    if (widget.listClsData != null) {
      listInstrClass = widget.listClsData;
    } else {
      if (widget.instrCode == null) {
        instrCode = await getInstrCode();
      } else {
        setState(() {
          instrCode = widget.instrCode;
        });
      }
    }
  }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      listInstrClass = null;
    });
  }

  _calDuration(String timefr, String timeto) {
    var tfr = timefr.split(':');
    var tto = timeto.split(':');
    int thr = int.parse(tto[0]) - int.parse(tfr[0]);
    int tmin = int.parse(tto[1]) - int.parse(tfr[1]);
    //convert hr to min
    var total = (thr * 60) + tmin;
    return (total).toString();
  }

  createClassListView(BuildContext context, AsyncSnapshot snapshot) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
      case ConnectionState.waiting:
        return Padding(
          padding: EdgeInsets.only(top: 200.0),
          child: Align(
              alignment: Alignment.topCenter,
              child: CircularProgressIndicator()),
        );
      case ConnectionState.done:
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else if (!snapshot.hasData) {
          return Text('');
        } else {
          return ListView.builder(
            itemBuilder: (context, index) {
              return _buildInstrClass(snapshot.data[index], snapshot.data);
              //return _getLists(context, snapshot.data, index);
            },
            itemCount: snapshot.data.length,
          );
          //return this._buildInstrClass(snapshot.data);
        }
        break;
      case ConnectionState.active:
        print("active");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
            appBar: AppBar(
                iconTheme: IconThemeData(
                  color: Colors.white, //change your color here
                ),
                title: Text("Today's Schedule",
                    style: TextStyle(color: Colors.white)),
                centerTitle: true,
                actions: <Widget>[_logOut()]),
            body: RefreshIndicator(
              child: Container(
                  child:
                      Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                _headerList(context, instrRes),
                _buildTableCalendar(),
                (listInstrClass == null)
                    ? Expanded(
                        flex: 3,
                        child: FutureBuilder(
                            future: Service.getInstrClass(
                                "InstrClass",
                                instrCode,
                                DateFormat('yyyyMMdd').format(_selectedDay)),
                            builder: (context, snapshot) {
                              return createClassListView(context, snapshot);
                            }))
                    : Expanded(
                        flex: 3,
                        child: ListView.builder(
                          itemBuilder: (context, index) {
                            return _buildInstrClass(
                                listInstrClass[index], listInstrClass);
                            //return _getLists(context, snapshot.data, index);
                          },
                          itemCount: listInstrClass.length,
                        )),
              ])),
              onRefresh: _handleRefresh,
            )));
  }

  Widget _logOut() => Padding(
      padding: EdgeInsets.only(top: 18.0, right: 10),
      child: GestureDetector(
          onTap: _onWillPop,
          child: Text("Logout",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w600))));

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Do you want to log out ?"),
          actions: <Widget>[
            FlatButton(
                child: Text("No"),
                onPressed: () => Navigator.pop(context, false)),
            FlatButton(
                child: Text("Yes"),
                onPressed: () => Navigator.popAndPushNamed(context, '/')),
          ],
        );
      },
    );
  }

  Future<Null> _handleRefresh() async {
    setState(() {
      Service.getInstrClass(
          "InstrClass", instrCode, DateFormat('yyyyMMdd').format(_selectedDay));
    });
  }

  Widget tLabel(String s) => Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Text(s, style: themeData.textTheme.display3));
  Widget nLabel(String s) => Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Text(
        s,
        style: themeData.textTheme.subtitle,
      ));
  Widget _dateTimeString(d) => Padding(
      padding: EdgeInsets.only(right: 40.0, top: 10.0),
      child: Text(
        d,
        style: themeData.textTheme.subtitle,
      ));
  Widget middleSection(InstrClassRes data) => Expanded(
        child: new Container(
          padding:
              new EdgeInsets.only(left: 15, top: 15, right: 15, bottom: 10),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Icon(
                CustomIcons.marathorn,
                color: themeData.primaryColor,
                size: 30,
              ),
              SizedBox(height: 10),
              new Row(
                children: <Widget>[
                  new Text(data.timeFrom,
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                      )),
                  SizedBox(width: 5),
                  new Text(
                    data.instrName,
                    style: new TextStyle(
                      color: Colors.grey,
                      fontSize: 14.0,
                    ),
                  )
                ],
              ),
              SizedBox(height: 5),
              new Text(
                data.className,
                style: new TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                ),
              )
            ],
          ),
        ),
      );
  Widget rightSection(InstrClassRes data) => Padding(
        padding: new EdgeInsets.only(bottom: 14, right: 20),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new Row(
              children: <Widget>[
                Text(
                  _calDuration(data.timeFrom, data.timeTo),
                  style: TextStyle(color: Colors.indigo[900], fontSize: 38.0),
                ),
                SizedBox(width: 10),
                Padding(
                  padding: new EdgeInsets.only(top: 10),
                  child: Text(
                    "Mins",
                    style: new TextStyle(color: Colors.indigo[900]),
                  ),
                )
              ],
            ),
            // Padding(
            //     padding: new EdgeInsets.only(left: 60),
            //     child: Text(
            //       "Complete",
            //       style: new TextStyle(
            //         color: themeData.primaryColor,
            //         fontWeight: FontWeight.w600,
            //         fontSize: 16.0,
            //       ),
            //     ))
          ],
        ),
      );

  Widget _headerList(BuildContext context, data) => Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(children: [
            tLabel('Trainer :'),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            nLabel((instrName == null) ? '' : instrName),
            _dateTimeString(DateFormat('dd/MM/yy').format(_selectedDay))
          ]),
        ],
      );

  Widget _buildTableCalendar() => TableCalendar(
        locale: 'en_US',
        initialCalendarFormat: CalendarFormat.week,
        formatAnimation: FormatAnimation.slide,
        startingDayOfWeek: StartingDayOfWeek.sunday,
        availableGestures: AvailableGestures.all,
        availableCalendarFormats: const {
          CalendarFormat.month: 'Month',
          CalendarFormat.week: 'Week',
        },
        calendarStyle: CalendarStyle(
          selectedColor: Colors.indigo[900],
          todayColor: Colors.blue[200],
          markersColor: Colors.brown[700],
        ),
        headerStyle: HeaderStyle(
          centerHeaderTitle: true,
          formatButtonVisible: true,
          formatButtonTextStyle:
              TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
          formatButtonDecoration: BoxDecoration(
            color: Colors.indigo[900],
            borderRadius: BorderRadius.circular(16.0),
          ),
        ),
        onDaySelected: _onDaySelected,
      );

  Widget _buildInstrClass(InstrClassRes data, List<InstrClassRes> allClsData) =>
      GestureDetector(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ClassAttendPage(
                    instrName: instrName,
                    reqData: new StdListReq(
                      data.classNo,
                      data.className,
                      data.instrName,
                      _selectedDay,
                      data.timeFrom,
                      data.timeTo,
                    ),
                    clsData: new InstrClassRes(
                        data.instrName,
                        data.classNo,
                        data.className,
                        data.timeFrom,
                        data.timeTo,
                        data.studAttNo,
                        data.studTotal),
                    listClsData: allClsData,
                  ),
            )),
        child: Padding(
          padding: EdgeInsets.only(left: 8.0, right: 8.0),
          child: Card(
            color: Colors.grey[200],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3),
            ),
            child: Row(
              children: <Widget>[middleSection(data), rightSection(data)],
            ),
          ),
        ),
      );
}
