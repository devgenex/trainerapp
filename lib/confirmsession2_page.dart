import 'package:flutter/material.dart';
import 'package:trainer_app/main.dart';
import 'package:intl/intl.dart';

import 'models/ClassAttdUpd.dart';
import 'models/InstrClass.dart';
import 'models/StdList.dart';
import 'schedule_page.dart';
import 'signature_page.dart';
import 'services/services.dart';

class ConfirmSessionPage2 extends StatefulWidget {
  final String instrName;
  final StdListReq reqData;
  final StdListRes resData;
  final List<InstrClassRes> listClsData;
  final String btnStatus;
  ConfirmSessionPage2(
      {Key key,
      this.instrName,
      this.reqData,
      this.resData,
      this.listClsData,
      this.btnStatus})
      : super(key: key);
  @override
  _ConfirmSessionState2 createState() => _ConfirmSessionState2();
}

class _ConfirmSessionState2 extends State<ConfirmSessionPage2> {
  NumberFormat curString;
  int _radioValue1 = 1;
  //fix disabled radio
  bool _isEnabled = true;

  @override
  void initState() {
    super.initState();
    //  setState(() {
    //   _radioValue1 = 0;
    // });
    curString = NumberFormat("#,##0.00", "en_US");
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
      switch (_radioValue1) {
        case 0:
          print(widget.btnStatus);
          break;
        case 1:
          print(1);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title:
              Text('Confirmed Session', style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        body: ListView(children: <Widget>[
          _buildStrDetail(widget.reqData, widget.resData),
        ]),
        bottomNavigationBar: BtnBottomScreen(
          reqData: widget.reqData,
          resData: widget.resData,
          btnStatus: widget.btnStatus,
          instrName: widget.instrName,
          listClsData: widget.listClsData,
        ));
  }

  Widget tLabel(String s) => Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Text(
        s,
        style: themeData.textTheme.subtitle,
      ));

  Widget nText(String s) => Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Text(
        s,
        style: themeData.textTheme.display1,
      ));

  Widget totalText() => Padding(
      padding: EdgeInsets.only(left: 100.0, top: 10.0),
      child: Text('TOTAL', style: themeData.textTheme.display3));

  Widget dText(String s) => Padding(
      padding: EdgeInsets.only(left: 30.0, top: 10.0),
      child: Text(
        s,
        style: themeData.textTheme.display1,
      ));

  Widget priceText(String s) => Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Text(
        ('\$') + s,
        style: TextStyle(
            color: themeData.primaryColor,
            fontSize: 20,
            fontWeight: FontWeight.bold),
      ));

  Widget timeText(String s) => Padding(
      padding: EdgeInsets.only(right: 30.0, top: 10.0),
      child: Text(
        s,
        style: TextStyle(
            color: Colors.indigo[900],
            fontSize: 40,
            fontWeight: FontWeight.bold),
      ));

  Widget _rowDetail(Widget label, Widget data) => Padding(
      padding: EdgeInsets.only(right: 20.0, bottom: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[label, data]));

  Widget _buildStrDetail(StdListReq reqData, StdListRes resData) {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 10.0, bottom: 10)),
        _rowDetail(tLabel('Class'), tLabel(reqData.className)),
        Divider(),
        _rowDetail(tLabel('Name'), tLabel(resData.studName)),
        Divider(),
        _rowDetail(tLabel('Remain Session'), tLabel(resData.quota)),
        Divider(),
        // Padding(
        //     padding: EdgeInsets.only(right: 20.0, bottom: 10),
        //     child: Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: <Widget>[
        //           tLabel('Payment Type'),
        //           priceText(curString.format(0))
        //         ])),
        ListTile(
          contentPadding: EdgeInsets.fromLTRB(19.0, 0, 0, 0),
          title: Text('Package',
              style: _isEnabled
                  ? themeData.textTheme.subtitle
                  : TextStyle(color: Colors.grey[500])),
          subtitle: Text('Pay with Package #',
              style: _isEnabled
                  ? TextStyle(color: Colors.black54)
                  : TextStyle(color: Colors.grey[500])),
          trailing: Radio(
            value: 1,
            groupValue: _radioValue1,
            onChanged: _isEnabled ? _handleRadioValueChange1 : null,
          ),
        ),
        Divider(),
      ],
    );
  }
}

class BtnBottomScreen extends StatelessWidget {
  final StdListReq reqData;
  final StdListRes resData;
  final String btnStatus;
  final String instrName;
  final List<InstrClassRes> listClsData;
  BtnBottomScreen(
      {Key key,
      this.reqData,
      this.resData,
      this.btnStatus,
      this.instrName,
      this.listClsData})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      height: 60.0,
      child: RaisedButton(
        //onPressed: () => confirmSessionBtn(context, btnStatus),
        onPressed: () =>
            switchRoute(context, btnStatus, instrName, listClsData),
        color: themeData.primaryColor,
        textColor: Colors.white,
        child: Text(
          'Confirm',
          style: TextStyle(
            fontSize: 18.0,
          ),
        ),
      ),
    );
  }

  void confirmSessionBtn(BuildContext context, String btnStatus,
      String instrName, List<InstrClassRes> listClsData) async {
    Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 20),
        content: Row(
          children: <Widget>[
            new CircularProgressIndicator(),
            new Text("  Confirming Session")
          ],
        )));

    var param = new ClassAttdUpdReq(
        DateFormat('yyyyMMdd').format(reqData.date),
        reqData.timeFr,
        reqData.timeTo,
        resData.bkNo,
        resData.cardNo,
        btnStatus);

    List<ClassAttdUpdRes> res =
        await Service.classAttdUpd("ClassAttdUpd", param);
    if (res[0].awsStatus == "OK") {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  SchedulePage(instrName: instrName, listClsData: listClsData)),
          ModalRoute.withName('/'));
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(res[0].message)));
    }
  }

  void switchRoute(context, btnStatus, instrName, listClsData) {
    (btnStatus == "S")
        ? Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SignaturePage(
                      instrName: instrName,
                      btnStatus: btnStatus,
                      reqData: new StdListReq(
                        reqData.classNo,
                        reqData.className,
                        reqData.instrName,
                        reqData.date,
                        reqData.timeFr,
                        reqData.timeTo,
                      ),
                      resData: new StdListRes(
                          resData.mg,
                          resData.memberNo,
                          resData.cardNo,
                          resData.studName,
                          resData.studAge,
                          resData.studMobile,
                          resData.studEmail,
                          resData.quota,
                          resData.attdStat,
                          resData.bkNo),
                      listClsData: listClsData,
                    )))
        : confirmSessionBtn(context, btnStatus, instrName, listClsData);
  }
}
