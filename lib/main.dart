import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trainer_app/login_page.dart';
import 'package:trainer_app/resetpassword_page.dart';
import 'package:trainer_app/schedule_page.dart';

import 'setting_page.dart';

void main() async {
  runApp(MyApp());
}

var themeData = ThemeData(
  fontFamily: 'Nunito',
  primaryColor: Colors.lightGreen[500],
  brightness: Brightness.light,
  backgroundColor: Colors.white,
  accentColor: Colors.lightGreen[500],
  cursorColor: Colors.lightGreen[500],
  textTheme: TextTheme(
    display3: TextStyle(
        color: Colors.grey[600], fontSize: 12, fontWeight: FontWeight.bold),
    subtitle: TextStyle(
        color: Colors.grey[600], fontSize: 16, fontWeight: FontWeight.bold),
    display1: TextStyle(
        color: Colors.grey[600], fontSize: 20, fontWeight: FontWeight.bold),
    display2: TextStyle(
        color: Colors.grey[600], fontSize: 14),
  ),
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Trainer App',
      theme: themeData,
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        '/schedule': (context) => SchedulePage(),
        '/setting': (context) => SettingPage(),
        //'/classattend': (context) => ClassAttendPage(),
        //'/confirmsession': (context) => ConfirmSessionPage(),
        '/resetpassword': (context) => ResetPasswordPage(),
      },
    );
  }
}

const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);
