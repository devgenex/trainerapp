class LoginRes {
  final String awsStatus;
  final String message;

  LoginRes(this.awsStatus, this.message);
}
