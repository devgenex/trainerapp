import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
//import 'package:printing/printing.dart';

const PdfColor green = PdfColor.fromInt(0xff9ce5d0);
const PdfColor lightGreen = PdfColor.fromInt(0xffcdf1e7);
const PdfColor grey = PdfColor.fromInt(0xff888888);

class MyPage extends Page {
  MyPage(
      {PdfPageFormat pageFormat = PdfPageFormat.a4,
      BuildCallback build,
      EdgeInsets margin})
      : super(pageFormat: pageFormat, margin: margin, build: build);

  @override
  void paint(Widget child, Context context) {
    context.canvas
      ..setColor(lightGreen)
      ..moveTo(0, pageFormat.height)
      ..lineTo(0, pageFormat.height - 230)
      ..lineTo(60, pageFormat.height)
      ..fillPath()
      ..setColor(green)
      ..moveTo(0, pageFormat.height)
      ..lineTo(0, pageFormat.height - 100)
      ..lineTo(100, pageFormat.height)
      ..fillPath()
      ..setColor(lightGreen)
      ..moveTo(30, pageFormat.height)
      ..lineTo(110, pageFormat.height - 50)
      ..lineTo(150, pageFormat.height)
      ..fillPath()
      ..moveTo(pageFormat.width, 0)
      ..lineTo(pageFormat.width, 230)
      ..lineTo(pageFormat.width - 60, 0)
      ..fillPath()
      ..setColor(green)
      ..moveTo(pageFormat.width, 0)
      ..lineTo(pageFormat.width, 100)
      ..lineTo(pageFormat.width - 100, 0)
      ..fillPath()
      ..setColor(lightGreen)
      ..moveTo(pageFormat.width - 30, 0)
      ..lineTo(pageFormat.width - 110, 50)
      ..lineTo(pageFormat.width - 150, 0)
      ..fillPath();

    super.paint(child, context);
  }
}

class Block extends StatelessWidget {
  Block({this.title});

  final String title;

  @override
  Widget build(Context context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Container(
              width: 6,
              height: 6,
              margin: const EdgeInsets.only(top: 2.5, left: 2, right: 5),
              decoration:
                  const BoxDecoration(color: green, shape: BoxShape.circle),
            ),
            Text(title,
                style: Theme.of(context)
                    .defaultTextStyle
                    .copyWith(fontWeight: FontWeight.bold)),
          ]),
          Container(
            decoration: const BoxDecoration(
                border: BoxBorder(left: true, color: green, width: 2)),
            padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
            margin: const EdgeInsets.only(left: 5),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Lorem(length: 20),
                ]),
          ),
        ]);
  }
}

class Category extends StatelessWidget {
  Category({this.title});

  final String title;

  @override
  Widget build(Context context) {
    return Container(
        decoration: const BoxDecoration(color: lightGreen, borderRadius: 6),
        margin: const EdgeInsets.only(bottom: 10, top: 20),
        padding: const EdgeInsets.fromLTRB(10, 7, 10, 4),
        child: Text(title, textScaleFactor: 1.5));
  }
}

Future<Document> generateDocument(
    PdfPageFormat format, ByteData byData, w, h, reqData, resData) async {
  final Document pdf = Document(deflate: zlib.encode);
  //PdfImage profileImage;
  PdfImage profileImage = PdfImage(pdf.document,
      image: byData.buffer.asUint8List(), width: w, height: h);

  pdf.addPage(pdfDesign(format, profileImage, reqData, resData));
  //   Page(
  //   pageFormat: format.applyMargin(
  //       left: 2.0 * PdfPageFormat.cm,
  //       top: 4.0 * PdfPageFormat.cm,
  //       right: 2.0 * PdfPageFormat.cm,
  //       bottom: 2.0 * PdfPageFormat.cm),
  //   build: (Context context) => Row(children: <Widget>[
  //         Expanded(
  //             child: Column(
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: <Widget>[
  //               Container(
  //                   padding: const EdgeInsets.only(left: 30, bottom: 20),
  //                   child: Column(
  //                       crossAxisAlignment: CrossAxisAlignment.start,
  //                       children: <Widget>[
  //                         Text('Parnella Charlesbois',
  //                             textScaleFactor: 2,
  //                             style: Theme.of(context)
  //                                 .defaultTextStyle
  //                                 .copyWith(fontWeight: FontWeight.bold)),
  //                         Padding(padding: const EdgeInsets.only(top: 10)),
  //                         Text('Electrotyper',
  //                             textScaleFactor: 1.2,
  //                             style: Theme.of(context)
  //                                 .defaultTextStyle
  //                                 .copyWith(
  //                                     fontWeight: FontWeight.bold,
  //                                     color: green)),
  //                         Padding(padding: const EdgeInsets.only(top: 20)),
  //                         Row(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                             children: <Widget>[
  //                               Column(
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.start,
  //                                   children: <Widget>[
  //                                     Text('568 Port Washington Road'),
  //                                     Text('Nordegg, AB T0M 2H0'),
  //                                     Text('Canada, ON'),
  //                                   ]),
  //                               Column(
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.start,
  //                                   children: <Widget>[
  //                                     Text('+1 403-721-6898'),
  //                                     Text('p.charlesbois@yahoo.com'),
  //                                     Text('wholeprices.ca')
  //                                   ]),
  //                               Padding(padding: EdgeInsets.zero)
  //                             ]),
  //                       ])),
  //               Category(title: 'Work Experience'),
  //               Block(title: 'Tour bus driver'),
  //               Block(title: 'Logging equipment operator'),
  //               Block(title: 'Foot doctor'),
  //               Category(title: 'Education'),
  //               Block(title: 'Bachelor Of Commerce'),
  //               Block(title: 'Bachelor Interior Design'),
  //               Block(
  //                   title:
  //                       '#################################################################'),
  //               Image(profileImage)
  //             ])),
  //         // Container(
  //         //     height: double.infinity,
  //         //     width: 100,
  //         //     decoration: const BoxDecoration(
  //         //         border: BoxBorder(left: true, color: green, width: 2)),
  //         //     child: Column(children: <Widget>[Image(profileImage)])),
  //       ]),
  // ));

  return pdf;
}

Widget tLabel(String s) => Text(
      s,
      style: TextStyle(color: grey, fontSize: 16, fontWeight: FontWeight.bold),
    );

Widget _rowDetail(Widget label, Widget data) => Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[label, data]);

Page pdfDesign(format, profileImage, reqData, resData) => Page(
      pageFormat: format.applyMargin(
          left: 2.0 * PdfPageFormat.cm,
          top: 4.0 * PdfPageFormat.cm,
          right: 2.0 * PdfPageFormat.cm,
          bottom: 2.0 * PdfPageFormat.cm),
      build: (Context context) => Container(
              child: Center(
            child: Padding(
                padding: new EdgeInsets.only(left: 20, right: 20),
                child: Column(children: <Widget>[
                  SizedBox(height: 20),
                  _rowDetail(
                      tLabel('Member / Guest #'), tLabel(resData.memberNo)),
                  SizedBox(height: 20),
                  // _rowDetail(tLabel('Name'), tLabel(widget.resData.studName)),
                  // SizedBox(height: 8),
                  _rowDetail(tLabel('Class'), tLabel(reqData.className)),
                  SizedBox(height: 10),
                  _rowDetail(tLabel('Name'), tLabel(resData.studName)),
                  SizedBox(height: 10),
                  _rowDetail(tLabel('Date'),
                      tLabel(DateFormat('dd/MM/yyyy').format(reqData.date))),
                  SizedBox(height: 10),
                  _rowDetail(tLabel('Payment'), tLabel("On Package")),
                  SizedBox(height: 50),
                  Container(
                      width: 150, height: 150, child: Image(profileImage)),
                  SizedBox(height: 20),
                  Container(child: tLabel("Verify With Signature"))
                ])),
          )),
    );
