class ClassAttdUpdReq {
  final String date;
  final String timeFr;
  final String timeTo;
  final String bkttNo;
  final String no;
  final String studAttd;

  ClassAttdUpdReq(this.date, this.timeFr, this.timeTo, this.bkttNo, this.no,
      this.studAttd);
}

class ClassAttdUpdRes {
  final String awsStatus;
  final String message;

  ClassAttdUpdRes(this.awsStatus, this.message);
}
