class InstrRes {
  final String awsStatus;
  final String message;
  final String instCode;
  final String instName;
  final String instEmail;
  
  InstrRes(this.awsStatus, this.message, this.instCode, this.instName,
      this.instEmail);
}
