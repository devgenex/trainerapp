class StdListReq {
  final String classNo;
  final String className;
  final String instrName;
  final DateTime date;
  final String timeFr;
  final String timeTo;

  StdListReq(this.classNo, this.className, this.instrName, this.date,
      this.timeFr, this.timeTo);
}

class StdListRes {
  final String mg;
  final String memberNo;
  final String cardNo;
  final String studName;
  final String studAge;
  final String studMobile;
  final String studEmail;
  final String quota;
  final String attdStat;
  final String bkNo;

  StdListRes(this.mg, this.memberNo, this.cardNo, this.studName, this.studAge,
      this.studMobile, this.studEmail, this.quota, this.attdStat, this.bkNo);
}

class ClsTimeCancelReq {
  final String bkNo;
  final DateTime date;

  ClsTimeCancelReq(this.bkNo, this.date);
}

class ClsTimeCancelRes {
  final String awsStatus;
  final String message;
  final String reCode;
  final String reDesc;

  ClsTimeCancelRes(this.awsStatus, this.message, this.reCode, this.reDesc);
}

class MemberPhotoReq {
  final String cardNumber;

  MemberPhotoReq(this.cardNumber);
}

class MemberPhotoRes {
  final String awsStatus;
  final String message;
  final String photo;

  MemberPhotoRes(this.awsStatus, this.message, this.photo);
}
