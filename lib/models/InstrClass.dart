class InstrClassReq {
  final String instrCode;
  final String date;

  InstrClassReq(this.instrCode, this.date);
}

class InstrClassRes {
  final String instrName;
  final String classNo;
  final String className;
  final String timeFrom;
  final String timeTo;
  final String studAttNo;
  final String studTotal;

  InstrClassRes(this.instrName, this.classNo, this.className, this.timeFrom,
      this.timeTo, this.studAttNo, this.studTotal);
}
