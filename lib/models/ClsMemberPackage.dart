class ClsMemberPackageReq {
  final String cardNo;

  ClsMemberPackageReq(this.cardNo);
}

class ClsMemberPackageRes {
  final String classNo;
  final String actiCode;
  final String voucBal;
  final String voucNum;
  final String voucDur;
  final String voucNo;
  final String cardNo;
  final String clasDesc;
  final String actiDesc;
  final String membName;

  ClsMemberPackageRes(
      this.classNo,
      this.actiCode,
      this.voucBal,
      this.voucNum,
      this.voucDur,
      this.voucNo,
      this.cardNo,
      this.clasDesc,
      this.actiDesc,
      this.membName);
}
