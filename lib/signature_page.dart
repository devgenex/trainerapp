import 'dart:io';
import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:pdf/pdf.dart';
import 'models/ClassAttdUpd.dart';
import 'models/InstrClass.dart';
import 'models/document.dart';
import 'main.dart';
import 'models/StdList.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'schedule_page.dart';
import 'services/services.dart';

const directoryName = 'Download';

class SignaturePage extends StatefulWidget {
  final String instrName;
  final StdListReq reqData;
  final StdListRes resData;
  final List<InstrClassRes> listClsData;
  final String btnStatus;
  SignaturePage(
      {Key key,
      this.instrName,
      this.reqData,
      this.resData,
      this.listClsData,
      this.btnStatus})
      : super(key: key);
  @override
  _SignatureState createState() => _SignatureState();
}

class _WatermarkPaint extends CustomPainter {
  final String price;
  final String watermark;

  _WatermarkPaint(this.price, this.watermark);

  @override
  void paint(ui.Canvas canvas, ui.Size size) {}

  @override
  bool shouldRepaint(_WatermarkPaint oldDelegate) {
    return oldDelegate != this;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _WatermarkPaint &&
          runtimeType == other.runtimeType &&
          price == other.price &&
          watermark == other.watermark;

  @override
  int get hashCode => price.hashCode ^ watermark.hashCode;
}

class _SignatureState extends State<SignaturePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ByteData _img = ByteData(0);
  var color = Colors.grey[600];
  var strokeWidth = 5.0;
  final _sign = GlobalKey<SignatureState>();
  Permission _permission = Permission.WriteExternalStorage;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _willPopCallback,
        child: Scaffold(
          key: _scaffoldKey,
          body: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Signature(
                      color: color,
                      key: _sign,
                      // onSign: () {
                      //   final sign = _sign.currentState;
                      //   debugPrint(
                      //       '${sign.points.length} points in the signature');
                      // },
                      backgroundPainter: _WatermarkPaint("2.0", "2.0"),
                      strokeWidth: strokeWidth,
                    ),
                  ),
                  color: Colors.black12,
                ),
              ),
              _img.buffer.lengthInBytes == 0
                  ? Container()
                  : LimitedBox(
                      maxHeight: 200.0,
                      child: Image.memory(_img.buffer.asUint8List())),
              Column(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                          child: RaisedButton(
                            color: themeData.primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            onPressed: () {
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                duration: Duration(seconds: 15),
                                content: Row(
                                  children: <Widget>[
                                    CircularProgressIndicator(),
                                    Text("  Loading...")
                                  ],
                                ),
                              ));
                              continueFnc(context);
                            },
                            padding: EdgeInsets.all(12),
                            child: Text('Confirm',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20)),
                          )),
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                          child: RaisedButton(
                            color: Colors.grey,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            onPressed: () => clearFnc(),
                            padding: EdgeInsets.all(12),
                            child: Text('Clear',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20)),
                          )),
                    ],
                  ),
                  //_optionSignature(),
                ],
              )
            ],
          ),
          floatingActionButton: Padding(
              padding: EdgeInsets.only(top: 120.0),
              child: FloatingActionButton(
                onPressed: () => backToConfirmPage(),
                //if you set mini to true then it will make your floating button small
                mini: false,
                child: new Icon(
                  Icons.clear,
                  color: Colors.white,
                ),
              )),
          floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        ));
  }

  Future<bool> _willPopCallback() async {
    Navigator.pop(context);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return true; // return true if the route to be popped
  }

  void backToConfirmPage() async {
    Navigator.pop(context);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  void continueFnc(BuildContext context) async {
    final sign = _sign.currentState;
    //retrieve image data, do whatever you want with it (send to server, save locally...)
    final image = await sign.getData();
    var byteDataforViewImage =
        await image.toByteData(format: ui.ImageByteFormat.png);
    var byteData = await image.toByteData(format: ui.ImageByteFormat.rawRgba);
    //SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    // _settingModalBottomSheet(context, byteDataforViewImage, byteData, image);
    //saveFile(byteData, image, widget.reqData, widget.resData);

    // var param = new ClassAttdUpdReq(
    //     DateFormat('dd-MM-yyyy').format(widget.reqData.date),
    //     widget.reqData.timeFr,
    //     widget.reqData.timeTo,
    //     widget.resData.bkNo,
    //     widget.resData.cardNo,
    //     widget.btnStatus);

    // List<ClassAttdUpdRes> res =
    //     await Service.classAttdUpd("ClassAttdUpd", param);
    // if (res[0].awsStatus == "OK") {
    //   await sendMail(byteData, image, widget.reqData, widget.resData);
    //   sign.clear();
    // } else {
    //   Scaffold.of(context)
    //       .showSnackBar(SnackBar(content: Text(res[0].message)));
    // }
    await sendMail(byteData, image, widget.reqData, widget.resData);
    sign.clear();
  }

  // Future<Null> saveFile(ByteData byteData, image, reqData, resData) async {
  //   if (!(await checkPermission())) await requestPermission();
  //   //Directory directory = await getTemporaryDirectory();
  //   Directory directory = await getExternalStorageDirectory();
  //   String path = directory.path;
  //   var filename = formattedDate();
  //   await Directory('$path/$directoryName').create(recursive: true);
  //   File('$path/$directoryName/$filename.pdf').writeAsBytesSync(
  //       (await generateDocument(PdfPageFormat.a4, byteData, image.width,
  //               image.height, widget.reqData, widget.resData))
  //           .save());
  //   Navigator.popAndPushNamed(context, '/schedule');
  //   SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  // }

  sendMail(ByteData byteData, image, reqData, resData) async {
    if (!(await checkPermission())) await requestPermission();
    Directory directory = await getTemporaryDirectory();
    //Directory directory = await getExternalStorageDirectory();
    String path = directory.path;

    var filename = formattedDate();

    await Directory('$path/$directoryName').create(recursive: true);
    File('$path/$directoryName/$filename.pdf').writeAsBytesSync(
        (await generateDocument(PdfPageFormat.a4, byteData, image.width,
                image.height, widget.reqData, widget.resData))
            .save());

    final smtpServer = gmail('bo@genex-solutions.com', 'C@rmengenex');
    var attachment =
        new FileAttachment(File('$path/$directoryName/$filename.pdf'));
    final message = new Message()
      ..from = new Address('bo@genex-solutions.com', 'TrainerApp')
      ..recipients.addAll([widget.resData.studEmail])
      //..recipients.addAll([BASE_EMAIL])
      ..subject = 'Test Dart Mailer library :: 😀 :: ${new DateTime.now()}'
      ..html = "<h1>Test</h1>\n<p>Hey! Here's some HTML content</p>"
      ..attachments.addAll([attachment]);

    final sendReports =
        await send(message, smtpServer, timeout: new Duration(seconds: 15));

    sendReports.forEach((sr) {
      if (sr.sent) {
        print('Message sent.');
      } else {
        print('Message not sent.');
        for (var p in sr.validationProblems) {
          print('Problem: ${p.code}: ${p.msg}');
        }
      }
    });

    var param = new ClassAttdUpdReq(
        DateFormat('yyyyMMdd').format(widget.reqData.date),
        widget.reqData.timeFr,
        widget.reqData.timeTo,
        widget.resData.bkNo,
        widget.resData.cardNo,
        widget.btnStatus);

    List<ClassAttdUpdRes> res =
        await Service.classAttdUpd("ClassAttdUpd", param);
    if (res[0].awsStatus == "OK") {
      _scaffoldKey.currentState.removeCurrentSnackBar();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => SchedulePage(
                  instrName: widget.instrName,
                  listClsData: widget.listClsData)),
          ModalRoute.withName('/'));
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(res[0].message)));
    }
  }

  requestPermission() async {
    PermissionStatus result =
        await SimplePermissions.requestPermission(_permission);
    return result;
  }

  checkPermission() async {
    bool result = await SimplePermissions.checkPermission(_permission);
    return result;
  }

  getPermissionStatus() async {
    final result = await SimplePermissions.getPermissionStatus(_permission);
    print("permission status is " + result.toString());
  }

  void clearFnc() async {
    final sign = _sign.currentState;
    sign.clear();
    setState(() {
      _img = ByteData(0);
    });
    debugPrint("cleared");
  }

  String formattedDate() {
    DateTime dateTime = DateTime.now();
    String dateTimeString = 'Signature_' +
        dateTime.year.toString() +
        dateTime.month.toString() +
        dateTime.day.toString() +
        dateTime.hour.toString() +
        ':' +
        dateTime.minute.toString() +
        ':' +
        dateTime.second.toString() +
        ':' +
        dateTime.millisecond.toString() +
        ':' +
        dateTime.microsecond.toString();
    return dateTimeString;
  }

  Widget _optionSignature() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MaterialButton(
              onPressed: () {
                setState(() {
                  color = color == Colors.green ? Colors.red : Colors.green;
                });
                debugPrint("change color");
              },
              child: Text("Change color")),
          MaterialButton(
              onPressed: () {
                setState(() {
                  int min = 1;
                  int max = 10;
                  int selection = min + (Random().nextInt(max - min));
                  strokeWidth = selection.roundToDouble();
                  debugPrint("change stroke width to $selection");
                });
              },
              child: Text("Change stroke width")),
        ],
      );

  Widget tLabel(String s) => Text(
        s,
        style: themeData.textTheme.subtitle,
      );
}
