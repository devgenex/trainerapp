import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:trainer_app/main.dart';
import 'package:intl/intl.dart';

import 'confirmsession2_page.dart';
import 'models/InstrClass.dart';
import 'models/StdList.dart';
import 'services/services.dart';

class ConfirmSessionPage extends StatefulWidget {
  final String instrName;
  final StdListReq reqData;
  final StdListRes resData;
  final List<InstrClassRes> listClsData;
  ConfirmSessionPage(
      {Key key, this.instrName, this.reqData, this.resData, this.listClsData})
      : super(key: key);
  @override
  _ConfirmSessionState createState() => _ConfirmSessionState();
}

class _ConfirmSessionState extends State<ConfirmSessionPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String btnStatus;
  NumberFormat curString;
  @override
  void initState() {
    super.initState();
    btnStatus = "";
    curString = NumberFormat("#,##0.00", "en_US");
  }

  createMemberView(
      BuildContext context, AsyncSnapshot snapshot, StdListRes resData) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
      case ConnectionState.waiting:
        return Align(
          alignment: Alignment.topCenter,
          child: CircularProgressIndicator(),
        );
      case ConnectionState.done:
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else if (!snapshot.hasData) {
          return Text('');
        } else {
          return ListView.builder(
            itemBuilder: (context, index) {
              return _buildPhotoView(snapshot.data[index], resData);
            },
            itemCount: snapshot.data.length,
          );
        }
        break;
      case ConnectionState.active:
        print("active");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title:
              Text('Confirmed Session', style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        body: ListView(
          children: <Widget>[
            _buildInstrDetail(widget.reqData),
            Container(
                height: 160,
                child: FutureBuilder(
                    future: Service.getmemberPhoto(
                        "MemberPhoto", widget.resData.cardNo),
                    builder: (context, snapshot) {
                      return createMemberView(
                          context, snapshot, widget.resData);
                    })),
            _buildStrDetail(widget.reqData, widget.resData),
          ],
        ),
        bottomNavigationBar: Row(children: <Widget>[
          _btnBottomScreen("Show Up", widget.resData.attdStat),
          _btnBottomScreen("No Show", widget.resData.attdStat)
        ]));
  }

  Widget tLabel(String s) => Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: (s.isNotEmpty)
          ? Text(s, style: themeData.textTheme.subtitle)
          : Text(""));

  Widget nText(String s) => Padding(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Text(
        s,
        style: themeData.textTheme.subtitle,
      ));

  Widget totalText() => Padding(
      padding: EdgeInsets.only(left: 120.0, top: 10.0),
      child: Text('TOTAL', style: themeData.textTheme.subtitle));

  Widget dText(String s) => Padding(
      padding: EdgeInsets.only(left: 30.0, top: 10.0),
      child: Text(
        s,
        style: themeData.textTheme.display1,
      ));

  Widget priceText(String s) => Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Text(
        ('\$') + s,
        style: themeData.textTheme.display1,
      ));

  Widget timeText(String s) => Padding(
      padding: EdgeInsets.only(right: 30.0, top: 10.0),
      child: Text(
        s,
        style: TextStyle(
            color: Colors.indigo[900],
            fontSize: 40,
            fontWeight: FontWeight.bold),
      ));

  Widget _buildInstrDetail(StdListReq data) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(children: [
          tLabel('Trianer :'),
        ]),
        Row(children: [
          nText((data == null) ? '' : data.instrName),
        ]),
        Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          dText(DateFormat('dd/MM/yy').format(data.date)),
          timeText(data.timeFr)
        ]),
      ],
    );
  }

  Widget _buildPhotoView(MemberPhotoRes data, StdListRes resData) {
// Image.memory(base64Decode(data.photo));
    return Center(
        child: Column(
      children: <Widget>[
        CircleAvatar(
          child: ClipOval(
            child: Image.memory(
              base64Decode(data.photo),
              width: 100,
              height: 100,
              fit: BoxFit.cover,
            ),
          ),
          //backgroundImage: NetworkImage(data.photo),
          radius: 36.0,
        ),
        SizedBox(height: 8),
        Text(
          resData.studName,
          style: TextStyle(
            color: Colors.indigo[900],
            fontSize: 18.0,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.phone,
              color: Colors.grey[300],
            ),
            Text(
              (resData.studMobile.isNotEmpty) ? resData.studMobile : " - ",
              style: TextStyle(color: Colors.grey),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.mail_outline,
              color: Colors.grey[300],
            ),
            Text(
              (resData.studEmail != null)
                  ? (resData.studEmail.isNotEmpty) ? resData.studEmail : " - "
                  : " - ",
              style: TextStyle(color: Colors.grey),
            ),
          ],
        ),
      ],
    ));
  }

  Widget _rowDetail(Widget label, Widget data) => Padding(
      padding: EdgeInsets.only(right: 20.0, bottom: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[label, data]));

  Widget _buildStrDetail(StdListReq reqData, StdListRes resData) {
    return Column(
      children: <Widget>[
        Divider(),
        _rowDetail(tLabel('Member / Guest #'), tLabel(resData.memberNo)),
        Divider(),
        _rowDetail(tLabel('Name'), tLabel(resData.studName)),
        Divider(),
        _rowDetail(tLabel('Class'), tLabel(reqData.className)),
        Divider(),
        // _rowDetail(tLabel('Mobile #'), tLabel(resData.studMobile)),
        // Divider(),
        _rowDetail(tLabel('Remain Session'), tLabel(resData.quota)),
        Divider(),
      ],
    );
  }

  Widget _btnBottomScreen(String textBtn, String attStatus) {
    return (attStatus == 'S')
        ? SizedBox.shrink()
        : ButtonTheme(
            height: 60.0,
            child: Expanded(
              child: OutlineButton(
                onPressed: () => widget.resData.quota.isNotEmpty
                    ? continueBottomBtn(textBtn)
                    : alertOutofQuota(context),
                color: Colors.white,
                textColor: (textBtn == "Show Up")
                    ? (widget.resData.quota.isNotEmpty)
                        ? Colors.indigo[900]
                        : Colors.grey
                    : (widget.resData.quota.isNotEmpty)
                        ? Colors.red
                        : Colors.grey,
                borderSide: BorderSide(color: Colors.transparent),
                child: Text(
                  textBtn,
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
            ));
  }

  void alertOutofQuota(BuildContext context) {
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text('No Remain Session')));
  }

  void continueBottomBtn(String textBtn) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ConfirmSessionPage2(
              instrName: widget.instrName,
              reqData: new StdListReq(
                widget.reqData.classNo,
                widget.reqData.className,
                widget.reqData.instrName,
                widget.reqData.date,
                widget.reqData.timeFr,
                widget.reqData.timeTo,
              ),
              resData: new StdListRes(
                  widget.resData.mg,
                  widget.resData.memberNo,
                  widget.resData.cardNo,
                  widget.resData.studName,
                  widget.resData.studAge,
                  widget.resData.studMobile,
                  widget.resData.studEmail,
                  widget.resData.quota,
                  widget.resData.attdStat,
                  widget.resData.bkNo),
              listClsData: widget.listClsData,
              btnStatus: (textBtn == "Show Up") ? "S" : "N"),
        ));
  }
}
