import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'main.dart';
import 'package:flutter_appavailability/flutter_appavailability.dart';

import 'models/InstrDetail.dart';
import 'services/services.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';

const String sharedSecret = 's3cr3t';

class ResetPasswordPage extends StatefulWidget {
  ResetPasswordPage({Key key}) : super(key: key);

  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _userNameFilter =
      new TextEditingController(text: "");
  String userName = "";

  void _userListen() {
    if (_userNameFilter.text.isEmpty) {
      userName = "";
    } else {
      userName = _userNameFilter.text;
    }
  }

  _ResetPasswordPageState() {
    _userNameFilter.addListener(_userListen);
  }

  onResetPassword(BuildContext context) async {
    List<InstrRes> instrRes =
        await Service.getInstrDetail("InstrDetail", userName);
    if (instrRes[0].awsStatus == "OK") {
      final jwt = senderCreatesJwt(userName, instrRes[0].instEmail);
      //final jwt = senderCreatesJwt(userName, BASE_EMAIL);
      final url = "$BASE_URL/resetpassword.asp?tk=$jwt";
      final smtpServer = gmail('bo@genex-solutions.com', 'C@rmengenex');
      final message = new Message()
        ..from = new Address('bo@genex-solutions.com', 'TrainerApp')
        ..recipients.addAll([instrRes[0].instEmail])
        //..recipients.addAll([BASE_EMAIL])
        ..subject = 'Reset your password'
        ..html =
            "<p>Please reset your password by clicking <a href=\"$url\">here</a>";
      await send(message, smtpServer, timeout: new Duration(seconds: 15));
      _scaffoldKey.currentState.removeCurrentSnackBar();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(instrRes[0].awsStatus),
            content: new Text("We’ve sent you email "),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(instrRes[0].awsStatus),
            content: new Text(instrRes[0].message),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  String senderCreatesJwt(String userName, String email) {
    // Create a claim set

    final claimSet = new JwtClaim(
        otherClaims: <String, dynamic>{'userName': userName, 'email': email},
        maxAge: const Duration(minutes: 30));

    // Generate a JWT from the claim set

    final token = issueJwtHS256(claimSet, sharedSecret);
    return token;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Text('Reset Password', style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        body: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 40.0, right: 40.0),
                child: Text("Enter your username below to reset password",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.grey[300],
                        fontSize: 20,
                        fontWeight: FontWeight.bold))),
            SizedBox(height: 45.0),
            TextFormField(
              controller: _userNameFilter,
              keyboardType: TextInputType.text,
              autofocus: false,
              decoration: InputDecoration(
                labelText: "Username",
                contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5),
              ),
            ),
            SizedBox(height: 35.0),
            Padding(
                padding: EdgeInsets.only(left: 80.0, right: 80.0),
                child: RaisedButton(
                  color: themeData.primaryColor,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  padding: EdgeInsets.all(12),
                  child: Text('Reset Password',
                      style: TextStyle(color: Colors.white, fontSize: 18)),
                  onPressed: () {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      duration: Duration(seconds: 7),
                      content: Row(
                        children: <Widget>[
                          CircularProgressIndicator(),
                          Text("  Loading...")
                        ],
                      ),
                    ));
                    onResetPassword(context);
                  },
                )),
            SizedBox(height: 60.0),
            Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: Text(
                    "We’ve sent you email with a link to reset your password.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.grey[300],
                        fontSize: 20,
                        fontWeight: FontWeight.bold))),
            SizedBox(height: 30.0),
            Padding(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
                child: GestureDetector(
                    child: Text("Check Email",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: themeData.primaryColor,
                            fontSize: 18)),
                    onTap: () {
                      openEmailApp(context);
                    })),
            SizedBox(height: 30.0),
            TestImage()
          ],
        ));
  }

  void openEmailApp(BuildContext context) {
    try {
      AppAvailability.launchApp(
              Platform.isIOS ? "message://" : "com.google.android.gm")
          .then((_) {
        print("App Email launched!");
      }).catchError((err) {
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text("App Email not found!")));
        print(err);
      });
    } catch (e) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("Email App not found!")));
    }
  }
}

class TestImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/resetpasswordimg.png');
    Image image = Image(image: assetImage);
    return Container(height: 180.0, width: 180.0, child: image);
  }
}
