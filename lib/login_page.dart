import 'dart:io';

import 'package:flutter/material.dart';
import 'package:trainer_app/main.dart';
import 'package:trainer_app/schedule_page.dart';
import 'package:trainer_app/services/services.dart';

import 'models/Login.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _userIdFilter =
      new TextEditingController(text: "");
  final TextEditingController _passwordFilter =
      new TextEditingController(text: "");

  String _userId = "";
  String _password = "";

  void _userListen() {
    if (_userIdFilter.text.isEmpty) {
      _userId = "";
    } else {
      _userId = _userIdFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  _LoginPageState() {
    _userIdFilter.addListener(_userListen);
    _passwordFilter.addListener(_passwordListen);
  }

  onLoginSubmit(BuildContext context) async {
    List<LoginRes> res =
        await Service.loginService("InstrLogin", _userId, _password);
    if (res[0].awsStatus == "OK") {
      saveCurrentLogin("/", _userId);
      _scaffoldKey.currentState.removeCurrentSnackBar();
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => SchedulePage(instrCode: _userId)));
    } else {
      _scaffoldKey.currentState.removeCurrentSnackBar();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(res[0].awsStatus),
            content: new Text(res[0].message),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  bool _rememberMeFlag = true;

  void _onRememberMeChanged(bool newValue) => setState(() {
        _rememberMeFlag = newValue;
        if (_rememberMeFlag) {
          // TODO: Here goes your functionality that remembers the user.
        } else {
          // TODO: Forget the user
        }
      });

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: Text(
        'ABC Fitness Club',
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 28, color: Colors.white),
      ),
    );

    final userId = TextFormField(
      controller: _userIdFilter,
      keyboardType: TextInputType.text,
      autofocus: false,
      style: new TextStyle(fontSize: 14, color: Colors.white),
      decoration: InputDecoration(
        hintText: 'UserId',
        hintStyle: TextStyle(color: Colors.white),
        prefixIcon: Icon(Icons.account_circle, size: 30, color: Colors.white),
        focusedBorder: const UnderlineInputBorder(
          borderSide: const BorderSide(color: Colors.white, width: 2.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 10.0),
      ),
    );

    final password = TextFormField(
      controller: _passwordFilter,
      autofocus: false,
      obscureText: true,
      style: new TextStyle(fontSize: 14, color: Colors.white),
      decoration: InputDecoration(
        hintText: 'Password',
        hintStyle: TextStyle(color: Colors.white),
        prefixIcon: Icon(Icons.lock, size: 30, color: Colors.white),
        focusedBorder: const UnderlineInputBorder(
          borderSide: const BorderSide(color: Colors.white, width: 2.0),
        ),
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 10.0),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: OutlineButton(
        highlightedBorderColor: themeData.primaryColor,
        borderSide: BorderSide(
          color: Colors.white, //Color of the border
          style: BorderStyle.solid, //Style of the border
          width: 1, //width of the border
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            duration: Duration(seconds: 7),
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                Text("  Loging In...")
              ],
            ),
          ));
          onLoginSubmit(context);
        },
        padding: EdgeInsets.all(10),
        child:
            Text('Log In', style: TextStyle(color: Colors.white, fontSize: 18)),
      ),
    );

    final rememberMe = new GestureDetector(
      onTap: () => setState(() => _rememberMeFlag = !_rememberMeFlag),
      child: Row(
        children: <Widget>[
          Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: Colors.white,
              ),
              child: Checkbox(
                value: _rememberMeFlag,
                //checkColor: Colors.yellowAccent, // color of tick Mark
                //activeColor: Colors.grey,
                onChanged: _onRememberMeChanged,
              )),
          new Text(
            "Remember",
            style: TextStyle(color: Colors.white, fontSize: 9),
          )
        ],
      ),
    );

    final forgotLabel = GestureDetector(
        onTap: () => Navigator.pushNamed(context, '/resetpassword'),
        child: Text(
          'Forgot password?',
          style: TextStyle(color: Colors.white, fontSize: 9),
        ));

    return Stack(
      children: <Widget>[
        Image.asset(
          "images/bglogingreen.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        WillPopScope(
            onWillPop: _onWillPop,
            child: Scaffold(
                key: _scaffoldKey,
                backgroundColor: Colors.transparent,
                body: Center(
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.only(left: 65.0, right: 65.0),
                    children: <Widget>[
                      SizedBox(height: 80.0),
                      logo,
                      SizedBox(height: 40.0),
                      userId,
                      SizedBox(height: 40.0),
                      password,
                      SizedBox(height: 40.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            rememberMe,
                            forgotLabel,
                          ]),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.25),
                      loginButton,
                    ],
                  ),
                ),
                floatingActionButton: FloatingActionButton(
                  onPressed: () => Navigator.pushNamed(context, '/setting'),
                  //if you set mini to true then it will make your floating button small
                  mini: false,
                  child: new Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                )))
      ],
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Are you sure?'),
          content: new Text('Do you want to exit an App'),
          actions: <Widget>[
            FlatButton(
                child: Text("No"),
                onPressed: () => Navigator.of(context).pop(false)),
            FlatButton(child: Text("Yes"), onPressed: () => exit(0)),
          ],
        );
      },
    );
  }
}
