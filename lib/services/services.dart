import 'package:http/http.dart' as http;
import 'package:trainer_app/models/ClassAttdUpd.dart';
import 'package:trainer_app/models/ClsMemberPackage.dart';
import 'package:trainer_app/models/InstrClass.dart';
import 'package:trainer_app/models/InstrDetail.dart';
import 'package:trainer_app/models/Login.dart';
import 'package:trainer_app/models/StdList.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml/xml.dart' as xml;

final String API_KEY = "<api_key>";
String BASE_URL = "http://aspen.genex-solutions.com:1002";
String BASE_EMAIL = "blueledgers@gmail.com";
final String pathName = "/webole.asp";
final String version = '''<?xml version="1.0" encoding="UTF-8"?>''';
final Object header = {"Content-Type": "text/xml; charset=utf-8"};

prepareReqXML(data) {
  var xml = '';
  xml += version;
  xml += '<RequestMessage ElementType="' + data["serviceName"] + '"> ';
  // Parameters
  for (var i = 0; i < data["reqMessage"].length; i++) {
    xml += ' <' +
        data["reqMessage"][i]["key"] +
        '>' +
        data["reqMessage"][i]["value"] +
        '</' +
        data["reqMessage"][i]["key"] +
        '> ';
  }
  xml += '</RequestMessage> ';
  return xml;
}

getValue(Iterable<xml.XmlElement> items) {
  var textValue;
  items.map((xml.XmlElement node) {
    textValue = node.text;
  }).toList();
  return textValue;
}

saveCurrentLogin(lastScreenRoute, userId) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('LastScreenRoute', lastScreenRoute);
  await prefs.setString('LastUserId', userId);
}

saveInstrName(instrName) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('InstrName', instrName);
}

getInstrName() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var instrName = prefs.getString('InstrName');
  return instrName;
}

getInstrCode() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var instrCode = prefs.getString('LastUserId');
  return instrCode;
}

updateBaseUrl(String newUrl) {
  BASE_URL = newUrl;
}

updateTestEmail(String newEmail) {
  BASE_EMAIL = newEmail;
}

class Service {
  static Future<dynamic> loginService(
      String serviceName, String username, String password) async {
    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {"key": "InstrCode", "value": username},
        {"key": "Password", "value": password}
      ]
    };

    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<LoginRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(LoginRes(getValue(item.findElements("AnswerStatus")),
          getValue(item.findElements("Message"))));
    }).toList();

    return itemsList;
  }

  static Future<dynamic> getInstrDetail(
      String serviceName, String userName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userCode = prefs.getString('LastUserId');

    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {
          "key": "InstrCode",
          "value": (userName == null || userName.isEmpty) ? userCode : userName
        }
      ]
    };

    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<InstrRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(InstrRes(
          getValue(item.findElements("AnswerStatus")),
          getValue(item.findElements("Message")),
          getValue(item.findElements("Inst_code")),
          getValue(item.findElements("Inst_name")),
          getValue(item.findElements("inst_email"))));
    }).toList();
    saveInstrName(itemsList[0].instName);
    return itemsList;
  }

  static Future<dynamic> getInstrClass(
      String serviceName, String userName, String date) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userCode = prefs.getString('LastUserId');
    //var fixdate = "20190518";
    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {
          "key": "InstrCode",
          "value": (userName == null || userName.isEmpty) ? userCode : userName
        },
        {"key": "Date", "value": date}
      ]
    };
    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<InstrClassRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(InstrClassRes(
          getValue(item.findElements("INSTRNAME")),
          getValue(item.findElements("CLASSNO")),
          getValue(item.findElements("CLAS_NAME")),
          getValue(item.findElements("TIMEFR")),
          getValue(item.findElements("TIMETO")),
          getValue(item.findElements("STUD_ATTNO")),
          getValue(item.findElements("STUD_TOTAL"))));
    }).toList();

    return itemsList;
  }

  static Future<dynamic> getStudList(
      String serviceName, StdListReq param, String date) async {
    //var fixclass = "PILP";
    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {"key": "Classno", "value": param.classNo},
        {"key": "Date", "value": date},
        {"key": "TimeFr", "value": param.timeFr},
        {"key": "TimeTo", "value": param.timeTo}
      ]
    };
    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<StdListRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(StdListRes(
        getValue(item.findElements("MG")),
        getValue(item.findElements("MBNUM")),
        getValue(item.findElements("CARDNO")),
        getValue(item.findElements("STUD_NAME")),
        getValue(item.findElements("STUD_AGE")),
        getValue(item.findElements("STUD_MOBI")),
        getValue(item.findElements("STUD_EMAIL")),
        getValue(item.findElements("PKG_QUOTA")),
        getValue(item.findElements("ATTD_STAT")),
        getValue(item.findElements("BKTTNO")),
      ));
    }).toList();
    return itemsList;
  }

  static Future<dynamic> cancelClsTime(
      String serviceName, String bkNo, String date) async {
    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {"key": "BKTTNO", "value": bkNo},
        {"key": "Date", "value": date},
      ]
    };
    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<ClsTimeCancelRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(ClsTimeCancelRes(
        getValue(item.findElements("AnswerStatus")),
        getValue(item.findElements("Message")),
        getValue(item.findElements("ATTD_STAT")),
        getValue(item.findElements("BKTTNO")),
      ));
    }).toList();
    return itemsList;
  }

  static Future<dynamic> getMemberPackage(
      String serviceName, String cardNo) async {
    //var fixclassNo = "A001P";
    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {"key": "Cardno", "value": cardNo},
      ]
    };
    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<ClsMemberPackageRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(ClsMemberPackageRes(
        getValue(item.findElements("CLASSNO")),
        getValue(item.findElements("ACTI_CODE")),
        getValue(item.findElements("VOUC_BAL")),
        getValue(item.findElements("VOUC_NUM")),
        getValue(item.findElements("VOUC_DUR")),
        getValue(item.findElements("VOUCNO")),
        getValue(item.findElements("CARDNO")),
        getValue(item.findElements("CLAS_DESC")),
        getValue(item.findElements("ACTI_DESC")),
        getValue(item.findElements("MEMB_NAME")),
      ));
    }).toList();
    return itemsList;
  }

  static Future<dynamic> getmemberPhoto(
      String serviceName, String cardNumber) async {
    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {"key": "CardNumber", "value": cardNumber}
      ]
    };
    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<MemberPhotoRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(MemberPhotoRes(
        getValue(item.findElements("AnswerStatus")),
        getValue(item.findElements("Message")),
        getValue(item.findElements("photo")),
      ));
    }).toList();
    return itemsList;
  }

  static Future<dynamic> classAttdUpd(
      String serviceName, ClassAttdUpdReq param) async {
    var student =
        '<Bkttno>${param.bkttNo}</Bkttno><No>${param.no}</No><Stud_Attd>${param.studAttd}</Stud_Attd>';

    var data = {
      "serviceName": serviceName,
      "reqMessage": [
        {"key": "Date", "value": param.date},
        {"key": "TimeFr", "value": param.timeFr},
        {"key": "TimeTo", "value": param.timeTo},
        {"key": "Student", "value": student},
      ]
    };
    var req = prepareReqXML(data);
    http.Response response =
        await http.post(BASE_URL + pathName, headers: header, body: req);
    var res = xml.parse(response.body.toString());
    List<ClassAttdUpdRes> itemsList = List();

    Iterable<xml.XmlElement> items = res.findAllElements('Response');
    items.map((xml.XmlElement item) {
      itemsList.add(ClassAttdUpdRes(
        getValue(item.findElements("AnswerStatus")),
        getValue(item.findElements("Message")),
      ));
    }).toList();
    return itemsList;
  }
}
