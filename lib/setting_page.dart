import 'package:flutter/material.dart';

import 'main.dart';
import 'services/services.dart';

class SettingPage extends StatefulWidget {
  SettingPage({Key key, this.title}) : super(key: key);
  final String title;

  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _serviceUrlFilter =
      new TextEditingController(text: BASE_URL);
  final TextEditingController _emailforTestFilter =
      new TextEditingController(text: BASE_EMAIL);
  String _serviceUrl = BASE_URL;
  String _email = BASE_EMAIL;

  void _serviceListen() {
    if (_serviceUrlFilter.text.isEmpty) {
      _serviceUrl = "";
    } else {
      _serviceUrl = _serviceUrlFilter.text;
    }
  }

  void _emailforTestListen() {
    if (_emailforTestFilter.text.isEmpty) {
      _email = "blueledgers@gmail.com";
    } else {
      _email = _emailforTestFilter.text;
    }
  }

  _SettingPageState() {
    _serviceUrlFilter.addListener(_serviceListen);
    _emailforTestFilter.addListener(_emailforTestListen);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Text("Setting", style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        body: Builder(
            // Create an inner BuildContext so that the onPressed methods
            // can refer to the Scaffold with Scaffold.of().
            builder: (BuildContext context) {
          return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                child: TextFormField(
                  controller: _serviceUrlFilter,
                  keyboardType: TextInputType.url,
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: "eg. $BASE_URL",
                    labelText: "API Url",
                    contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                  ),
                )),
            // Padding(
            //     padding: EdgeInsets.all(30),
            //     child: TextFormField(
            //       controller: _emailforTestFilter,
            //       keyboardType: TextInputType.emailAddress,
            //       autofocus: false,
            //       decoration: InputDecoration(
            //         labelText: "Email for test sendmail",
            //         hintText: "Email for test sendmail",
            //         contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5),
            //       ),
            //     )),
            Padding(
                padding: EdgeInsets.all(10),
                child: RaisedButton(
                  color: themeData.primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24),
                  ),
                  onPressed: () {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      duration: Duration(seconds: 15),
                      content: Row(
                        children: <Widget>[
                          CircularProgressIndicator(),
                          Text("  Saving...")
                        ],
                      ),
                    ));
                    _update(context, _serviceUrl, _email);
                  },
                  padding: EdgeInsets.all(12),
                  child: Text('Save',
                      style: TextStyle(color: Colors.white, fontSize: 20)),
                )),
          ]);
        }));
  }

  void _update(BuildContext context, String _serviceUrl, String _email) {
    updateBaseUrl(_serviceUrl);
    updateTestEmail(_email);
    Future.delayed(const Duration(milliseconds: 2000), () {
      Navigator.pop(context);
    });
  }
}
