import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:trainer_app/models/InstrClass.dart';
import 'package:trainer_app/services/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'main.dart';
import 'models/ClsMemberPackage.dart';
import 'models/StdList.dart';
import 'confirmsession_page.dart';

class ClassAttendPage extends StatefulWidget {
  final String instrName;
  final StdListReq reqData;
  final StdListRes resData;
  final InstrClassRes clsData;
  final List<InstrClassRes> listClsData;
  ClassAttendPage(
      {Key key,
      this.instrName,
      this.reqData,
      this.resData,
      this.clsData,
      this.listClsData})
      : super(key: key);
  @override
  _ClassAttendPageState createState() => _ClassAttendPageState();
}

class _ClassAttendPageState extends State<ClassAttendPage>
    with TickerProviderStateMixin {
  NumberFormat curString;
  dynamic packagePrice;
  List<ClsMemberPackageRes> clsMemberRes;

  @override
  void initState() {
    super.initState();
    curString = NumberFormat("#,##0.00", "en_US");
  }

  void cancelBkk(BuildContext context, String bkkNo) async {
    List<ClsTimeCancelRes> res = await Service.cancelClsTime("ClsTimeCancel",
        bkkNo, DateFormat('yyyyMMdd').format(widget.reqData.date));
    if (res[0].awsStatus == "OK") {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(res[0].reDesc)));
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(res[0].message)));
    }
  }

  // createStdDetail(BuildContext context, AsyncSnapshot snapshot) {
  //   switch (snapshot.connectionState) {
  //     case ConnectionState.none:
  //     case ConnectionState.waiting:
  //     // return Padding(
  //     //   padding: EdgeInsets.only(top: 200.0),
  //     //   child: Align(
  //     //       alignment: Alignment.topCenter,
  //     //       child: CircularProgressIndicator()),
  //     // );
  //     case ConnectionState.done:
  //       if (snapshot.hasError) {
  //         return Text('Error: ${snapshot.error}');
  //       } else if (!snapshot.hasData) {
  //         return Text('');
  //       } else {
  //         return _headerList(context, snapshot.data);
  //       }
  //       break;
  //     case ConnectionState.active:
  //       print("active");
  //       break;
  //   }
  // }

  createStdListView(BuildContext context, AsyncSnapshot snapshot) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
      case ConnectionState.waiting:
        return Padding(
          padding: EdgeInsets.only(top: 200.0),
          child: Align(
              alignment: Alignment.topCenter,
              child: CircularProgressIndicator()),
        );
      case ConnectionState.done:
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else if (!snapshot.hasData) {
          return Text('');
        } else {
          return ListView.builder(
            itemBuilder: (context, index) {
              return _getSlidableWithLists(context, snapshot.data, index);
            },
            itemCount: snapshot.data.length,
          );
        }
        break;
      case ConnectionState.active:
        print("active");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.white, //change your color here
            ),
            title:
                Text('Class Attendance', style: TextStyle(color: Colors.white)),
            centerTitle: true,
            actions: <Widget>[_logOut()]),
        body: RefreshIndicator(
          child: Container(
              child: Column(children: <Widget>[
            // Flexible(
            //     child: FutureBuilder(
            //         future:
            //             Service.getMemberPackage("ClsMemberPackage", "test"),
            //         builder: (context, snapshot) {
            //           return createStdDetail(context, snapshot);
            //         })),
            _headerList(context),
            Expanded(
                flex: 2,
                child: FutureBuilder(
                    future: Service.getStudList("StudList", widget.reqData,
                        DateFormat('yyyyMMdd').format(widget.reqData.date)),
                    builder: (context, snapshot) {
                      return createStdListView(context, snapshot);
                    })),
          ])),
          onRefresh: _handleRefresh,
        ));
  }

  Widget _logOut() => Padding(
      padding: EdgeInsets.only(top: 18.0, right: 10),
      child: GestureDetector(
          onTap: _onWillPop,
          child: Text("Logout",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w600))));

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Do you want to log out ?"),
          actions: <Widget>[
            FlatButton(
                child: Text("No"),
                onPressed: () => Navigator.pop(context, false)),
            FlatButton(
                child: Text("Yes"),
                onPressed: () => Navigator.popAndPushNamed(context, '/')),
          ],
        );
      },
    );
  }

  Future<Null> _handleRefresh() async {
    setState(() {
      Service.getStudList("StudList", widget.reqData,
          DateFormat('yyyyMMdd').format(widget.reqData.date));
    });
  }

  findPricePackage(List<ClsMemberPackageRes> data) =>
      data.firstWhere((x) => (x.actiCode == widget.reqData.classNo)).voucBal;

  Widget _headerList(BuildContext context) =>
      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Row(children: [
          iLabel('Instructor :'),
        ]),
        Row(children: [
          ivLabel(widget.reqData.instrName),
        ]),
        Row(children: [
          iLabel('Class :'),
        ]),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          nLabel(widget.reqData.className),
          // priceText(curString.format(0)),
          // Padding(
          //     padding: EdgeInsets.only(left: 5, top: 10, right: 25),
          //     child: Icon(
          //       Icons.supervisor_account,
          //       //CustomIcons.marathorn,
          //       color: themeData.primaryColor,
          //     )),
        ]),
        Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          dLabel(DateFormat('dd/MM/yy').format(widget.reqData.date)),
          tLabel(widget.reqData.timeFr),
        ]),
        //Divider(),
      ]);

  Widget _buildStudList(StdListRes data) => GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ConfirmSessionPage(
                    instrName: widget.instrName,
                    reqData: StdListReq(
                      widget.reqData.classNo,
                      widget.reqData.className,
                      widget.reqData.instrName,
                      widget.reqData.date,
                      widget.reqData.timeFr,
                      widget.reqData.timeTo,
                    ),
                    resData: StdListRes(
                        data.mg,
                        data.memberNo,
                        data.cardNo,
                        data.studName,
                        data.studAge,
                        data.studMobile,
                        data.studEmail,
                        data.quota,
                        data.attdStat,
                        data.bkNo),
                    listClsData: widget.listClsData,
                  ))),
      child: Padding(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: Row(
          children: <Widget>[
            //_leftSection(),
            _middleSection(data),
            _rightSection(data),
          ],
        ),
      ));
  Widget _middleSection(StdListRes data) => Expanded(
        child: Container(
          padding: EdgeInsets.only(left: 15, top: 15, right: 15, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                data.studName,
                style: TextStyle(
                  color: Colors.indigo[900],
                  fontSize: 18.0,
                ),
              ),
              Row(
                children: <Widget>[
                  Text('Age : ', style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(
                    data.studAge + ' |',
                    style: themeData.textTheme.display2,
                  ),
                  Text(' Nickname : ',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(
                    ' - ',
                    style: themeData.textTheme.display2,
                  )
                ],
              ),
              Row(children: <Widget>[
                Icon(
                  Icons.phone,
                  //CustomIcons.marathorn,
                  color: Colors.grey[300],
                ),
                (data.studMobile.isNotEmpty)
                    ? Text(
                        ' ' + data.studMobile,
                        style: themeData.textTheme.display2,
                      )
                    : Text(
                        ' - ',
                        style: themeData.textTheme.display2,
                      )
              ]),
            ],
          ),
        ),
      );
  Widget _rightSection(StdListRes data) => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            children: <Widget>[
              (data.attdStat == "S")
                  ? Text(
                      "Show Up",
                      style: new TextStyle(
                        color: Colors.indigo[900],
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                      ),
                    )
                  : Text(
                      "Booking",
                      style: new TextStyle(
                        color: Colors.indigo[900],
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                      ),
                    ),
              SizedBox(width: 10),
              (data.studMobile.isNotEmpty)
                  ? GestureDetector(
                      onTap: () => launch('tel://' + data.studMobile),
                      child: Icon(
                        Icons.phone,
                        //CustomIcons.marathorn,
                        color: themeData.primaryColor,
                        size: 30,
                      ))
                  : Icon(
                      Icons.phone,
                      //CustomIcons.marathorn,
                      color: Colors.grey[300],
                      size: 30,
                    ),
              SizedBox(width: 10),
            ],
          )
        ],
      );

  Widget _getSlidableWithLists(
      BuildContext context, List<StdListRes> data, int index) {
    return (data[index].attdStat != "X")
        ? Slidable(
            key: Key(data[index].memberNo),
            dismissal: SlidableDismissal(
              child: SlidableDrawerDismissal(),
              onDismissed: (actionType) {
                (data[index].attdStat == "B")
                    ? setState(() {
                        cancelBkk(context, data[index].bkNo);
                      })
                    : showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Cancel"),
                            content: Text("Can't cancel this student"),
                            actions: <Widget>[
                              // usually buttons at the bottom of the dialog
                              new FlatButton(
                                child: new Text("Close"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
              },
            ),
            child: _buildStudList(data[index]),
            actionPane: SlidableScrollActionPane(),
            secondaryActions: <Widget>[
              IconSlideAction(
                caption: 'Cancel',
                color: Colors.red,
                icon: Icons.delete,
                onTap: () => (data[index].attdStat == "B")
                    ? cancelBkk(context, data[index].bkNo)
                    : showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Cancel"),
                            content: Text("Can't cancel this student"),
                            actions: <Widget>[
                              // usually buttons at the bottom of the dialog
                              new FlatButton(
                                child: new Text("Close"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      ),
              ),
            ],
          )
        : Text("");
  }
}

Widget iLabel(String s) => Padding(
    padding: EdgeInsets.only(left: 20.0, top: 10.0),
    child: Text(s, style: themeData.textTheme.display3));
Widget ivLabel(String s) => Padding(
    padding: EdgeInsets.only(left: 30.0, top: 10.0),
    child: Text(s, style: themeData.textTheme.subtitle));
Widget nLabel(String s) => Flexible(
      child: Container(
        padding: EdgeInsets.only(left: 30.0, top: 10.0),
        child: Text(s,
            overflow: TextOverflow.ellipsis,
            style: themeData.textTheme.subtitle),
      ),
    );
Widget priceText(String s) => Padding(
    padding: EdgeInsets.only(left: 10.0, top: 10.0),
    child: Text(
      ('\$') + s,
      style: TextStyle(
          color: themeData.primaryColor,
          fontSize: 20,
          fontWeight: FontWeight.bold),
    ));
Widget dLabel(String s) => Padding(
    padding: EdgeInsets.only(top: 10.0),
    child: Text(s, style: themeData.textTheme.display1));
Widget tLabel(String s) => Padding(
    padding: EdgeInsets.only(top: 10.0),
    child: Text(
      s,
      style: TextStyle(
          color: Colors.indigo[900], fontSize: 36, fontWeight: FontWeight.bold),
    ));
